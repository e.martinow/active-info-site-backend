"""active_info_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import login, logout
from rest_framework import routers
from app import views as app_views
from rest_framework.authtoken import views


router = routers.DefaultRouter()
# router.register(r'users', app_views.UserViewSet)
# router.register(r'groups', app_views.GroupViewSet)
router.register(r'users', app_views.UserViewSet, basename='user')
router.register(r'user/create', app_views.UserCreationViewSet, basename='create_user')
router.register(r'user/password', app_views.PasswordChangeViewSet, basename='change_password')

router.register(r'auth/getcode', app_views.GetCodeViewSet, basename='getcode')
router.register(r'auth/verifycode', app_views.VerifyCodeViewSet, basename='getcode')
router.register(r'profile', app_views.ProfileViewSet, basename='profile')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^test/', app_views.test),
    path('api/v0/', include(router.urls)),
    url(r'^api/v0/auth/login', app_views.CustomAuthToken.as_view()),
    url(r'^api/v0/auth/token', views.obtain_auth_token)
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]



