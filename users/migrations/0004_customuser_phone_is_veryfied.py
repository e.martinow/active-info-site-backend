# Generated by Django 3.0.6 on 2020-05-10 20:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20200510_1935'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='phone_is_veryfied',
            field=models.BooleanField(default=False),
        ),
    ]
