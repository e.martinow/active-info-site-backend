----ИНСТРУЦИЯ ПО РАЗВЕРТЫВАНИЮ----

OS: Linux

В системе должен быть установлен python3 (идет в стандартном пакете Ubuntu) и postgres.

1. Установка и настройка postgresql (Ubuntu):

sudo apt install psql

sudo -u postgres psql
    
create database ulstu_ais;
create user ulstu with password '89279876093';
grant all privileges on 'ulstu_ais' to ulstu;
    
2. Создание виртуального окружения

python3 -m venv venv (создастся папка venv, содержащая питоновский интерпретатор и другие свистелки. Вместо venv монжо указать любой адрес)

3. Активация виртуального окружения

source venv/bin/activate

4. Клонирование проекта

В желаемой папаке делаем git clone git@gitlab.com:e.martinow/active-info-site-backend.git . (или git clone https://gitlab.com/e.martinow/active-info-site-backend.git .) 
Я обычно клонирую проект внутрь виртулаьного окружения, чтобы знать какое окружение к какому проекту относится, посокльку их может быть много.

----
Пункты ниже выполнять в папке проекта при активированном виртуальном окружении (sic!) - см. пункт №3
----
5. Установка зависимостей

pip install -r requirements.txt

6. Применение миграций 

python manage.py migrate

Если миграции выполняются 1-ый раз (на чистую БД), стоит выполнить

python manage.py createsuperuser

Это создаст суперпользователя для работы с админкой

7. Запуск development сервера

python manage.py runserver (по дефолту запускается на порту 8000, если он занят, то порт указывается через пробел - manage.py runserver 8001)

Приложение запущено.


----
Глобальные
----

Запрос /admin/ в браузере откроет админ-панель.
Все api запросы обрабатываются относительно /api/v0/.
Rest Framework имеет свой интерфейс просмотра и тестирования урл. Его можно открыть в бразуере, отправив любой api-запрос. 
Из корневого запроса /api/v0/ можно увидеть все дочерние запросы.



