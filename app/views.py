from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.utils import json

from .serializers import UserSerializer, CustomAuthTokenSerializer, ProfileSerializer
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status, response
from django.utils import timezone
from datetime import timedelta
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from django.conf import settings
from pprint import pprint
import random

from users.models import CustomUser as User
from app.models import Sms, Profile
from .utils import phone_validation, sms_check


class UserViewSet(viewsets.ViewSet):
    """
    Получение списка или конкретного юзера
    """
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request):
        queryset = User.objects.all().order_by('-date_joined')
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)


class UserCreationViewSet(viewsets.ViewSet):
    def create(self, request):
        """
        Создание пользователя
        Формат номера +7XXXXXXXXXX
        """
        phone = request.data.get("phone")
        password = request.data.get("password")
        if phone_validation(phone):
            try:
                user = User.objects.create_user(phone, password)
                token, created = Token.objects.get_or_create(user=user)
                return Response({
                    'token': token.key,
                    'user_id': user.pk,
                    'phone': user.phone
                }, status=status.HTTP_200_OK)
            except:
                return Response({'message': 'Пользователь уже существует!',
                                 'detail': 'Войдите в систему'},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': 'Неверный формат номера телефона!',
                             'detail': 'Введите корректный номер формата +7XXXXXXXXXX'},
                            status=status.HTTP_400_BAD_REQUEST)


class GetCodeViewSet(viewsets.ViewSet):
    """
    Запрос на получение смс с кодом
    Формат номера +7XXXXXXXXXX
    """
    def create(self, request):
        phone = request.data.get("phone")
        if User.objects.filter(phone=phone, is_active=False).exists():
            return Response({'message': 'Ваш аккаунт заблокирован',
                             'detail': 'Обратитесь к администратору'}, status=status.HTTP_403_FORBIDDEN)

        if phone_validation(phone):
            code = random.randint(1000, 9999)
            try:
                sms = Sms.objects.get(phone=phone)
                if timezone.now() - sms.cdate < timedelta(seconds=settings.SMS_TIMEOUT):
                    return Response({'message': 'Сообщение уже было отправлено!',
                                     'detail': 'Между отправками смс должно пройти ' + str(settings.SMS_TIMEOUT) +
                                               ' секунд'},
                                    status=status.HTTP_403_FORBIDDEN)
            except ObjectDoesNotExist:
                Sms.objects.create(phone=phone, code=code).save()

            return Response({'message': 'Успешная отправка!',
                             'detail': 'Код подтверждения был выслан на указанный номер'},
                            status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Неверный формат номера телефона!',
                             'detail': 'Введите корректный номер формата +7XXXXXXXXXX'},
                            status=status.HTTP_400_BAD_REQUEST)


class VerifyCodeViewSet(viewsets.ViewSet):
    """
    Запрос на проверку кода из смс
    """
    def create(self, request):
        code = request.data.get("code")
        phone = request.data.get("phone")
        res = sms_check(phone, code)
        if res.status_code == 200:
            return Response({'message': 'Код верный!',
                             'detail': 'Номер подтвержден'},
                            status=status.HTTP_200_OK)
        else:
            return res


class CustomAuthToken(ObtainAuthToken):
    """
    Переопределение стандартного лоигна rest_framework
    используется phone вместо username
    """
    def post(self, request, *args, **kwargs):
        serializer = CustomAuthTokenSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if not user.is_active:
            return Response({'message': 'Ваш аккаунт заблокирован',
                             'detail': 'Обратитесь к администратору'}, status=status.HTTP_403_FORBIDDEN)
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'phone': user.phone
        }, status=status.HTTP_200_OK)


class ProfileViewSet(viewsets.ModelViewSet):
    """
    Просмотр списка, создание и обновление профиля
    """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProfileSerializer
    http_method_names = ['get', 'options', 'post', 'put', 'patch']

    def get_queryset(self):
        queryset = Profile.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except:
            return Response({'message': 'Профиль уже существует!',
                             'detail': 'Вы не можете создать несколько профилей'}, status=status.HTTP_403_FORBIDDEN)

    def update(self, request, *args, **kwargs):
        profile = self.get_object()
        serializer = self.get_serializer(profile, data=request.data, partial=True)
        if serializer.is_valid(raise_exception=True):
            profile = self.perform_update(serializer)
            return Response(serializer.data)
        else:
            return Response({'message': 'Ошибка!',
                             'detail': serializer.errors}, status=status.HTTP_403_FORBIDDEN)

    def list(self, request, *args, **kwargs):
        queryset = Profile.objects.get(user=self.request.user)
        serializer = self.serializer_class(queryset)
        return response.Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PasswordChangeViewSet(viewsets.ModelViewSet):
    """
    Сброс пароля
    Необходимо сперва отправить getcode
    """
    def create(self, request, *args, **kwargs):
        phone = request.data.get("phone")
        code = request.data.get("code")
        password1 = request.data.get("password1")
        password2 = request.data.get("password2")
        try:
            user = User.objects.get(phone=phone)
        except ObjectDoesNotExist:
            return Response({'message': 'Пользователь не найден!',
                             'detail': 'На указанный номер нет зарегистрированных пользователей'},
                            status=status.HTTP_400_BAD_REQUEST)
        res = sms_check(phone, code)
        if res.status_code == 200:
            if password1 == password2:
                user.set_password(password1)
                user.save()
                return Response({'message': 'Успешно!',
                                 'detail': 'Пароль успешно изменен'},
                                status=status.HTTP_200_OK)
            else:
                return Response({'message': 'Пароли не совпадают!',
                                 'detail': 'Проверьте введенные данные'},
                                status=status.HTTP_403_FORBIDDEN)
        else:
            return res
