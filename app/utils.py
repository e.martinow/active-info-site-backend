from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response

from users.models import CustomUser as User
from app.models import Sms


def phone_validation(phone):
    if len(phone) != 12 or not phone.startswith('+7'):
        return False
    return True


def code_validation(code):
    if len(code) != 4 or code != code.lower():
        return False
    return True


def sms_check(phone, code):
    if code_validation(code):
        try:
            sms = Sms.objects.get(phone=phone)
            if timezone.now() - sms.cdate >= timedelta(seconds=settings.SMS_LIFETIME * 60):
                # обсудить случай устаревания кода
                code = random.randint(1000, 9999)
                sms.code = code
                sms.save()
                return Response({'message': 'Код устарел!',
                                 'detail': 'Вам был выслан новый код'},
                                status=status.HTTP_403_FORBIDDEN)
            if code == sms.code:
                try:
                    user = User.objects.get(phone=phone)
                    user.phone_is_veryfied = True
                    user.save()
                    sms.delete()
                    return Response({'message': 'Код верный',
                                     'detail': 'Получено подтверждение'},
                                    status=status.HTTP_200_OK)
                except ObjectDoesNotExist:
                    return Response({'message': 'Пользователь не найден!',
                                     'detail': 'Пользователь с таким телефонным номером не существует'},
                                    status=status.HTTP_400_BAD_REQUEST)

            else:
                return Response({'message': 'Неверный код!',
                                 'detail': 'Введите код из смс'},
                                status=status.HTTP_403_FORBIDDEN)
        except ObjectDoesNotExist:
            return Response({'message': 'Код не найден!',
                             'detail': 'На этот номер смс не отправлялось. Попробуйте еще.'},
                            status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({'message': 'Неверный формат кода!',
                         'detail': 'Введите код из смс'},
                        status=status.HTTP_400_BAD_REQUEST)