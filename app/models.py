from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

# Create your models here.


class Sms(models.Model):
    code = models.CharField(_('Код'), max_length=4)
    phone = models.CharField(_('Мобильный номер'), max_length=15, unique=True)
    cdate = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = 'СМС код'
        verbose_name_plural = 'СМС коды'
        ordering = ['-cdate']


class Profile(models.Model):
    user = models.OneToOneField('users.CustomUser', on_delete=models.CASCADE, verbose_name='Пользователь')
    first_name = models.CharField(_('Имя'), max_length=40)
    last_name = models.CharField(_('Фамилия'), max_length=40)
    school = models.CharField(_('Школа'), max_length=255)
    info = models.CharField(_('О себе'), max_length=255)

    class Meta:
        verbose_name = 'Профили пользователей'
        verbose_name_plural = 'Профиль пользователя'
        ordering = ['user']
