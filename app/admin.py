from django.contrib import admin
from .models import Profile, Sms


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'last_name')
    search_fields = ('user',)


admin.site.register(Profile, ProfileAdmin)


class SmsAdmin(admin.ModelAdmin):
    list_display = ('phone', 'code', 'cdate')
    search_fields = ('phone','cdate')
    ordering = ['-cdate']


admin.site.register(Sms, SmsAdmin)

# Register your models here.
